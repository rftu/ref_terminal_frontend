import Vue from 'vue';
import VueHead from 'vue-head';
import VueResource from 'vue-resource';
import { Snotify } from 'vue-snotify';
import Affix from 'vue-affix';

import { root } from './config';

Vue.use(VueHead);
Vue.use(VueResource);
Vue.use(Snotify);
Vue.use(Affix);

Vue.http.options.root = root + '/api';

