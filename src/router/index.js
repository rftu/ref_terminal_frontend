import Vue from 'vue';
import Router from 'vue-router';

const PageContainer = () => import('@/components/PageContainer');
const Catalog = () => import('@/components/Catalog/Catalog');
const ManagerCard = () => import('@/components/Manager/ManagerCard');
const ContainerEdit = () => import('@/components/Container/ContainerEdit');
const ContainerSale = () => import('@/components/Container/ContainerSale');
const Cart = () => import('@/components/Cart/Cart');
const CartCheck = () => import('@/components/Cart/CartCheck');
const CartExecute = () => import('@/components/Cart/CartExecute');
const CartPay = () => import('@/components/Cart/CartPay');
const Faq = () => import('@/components/Other/Faq');
const Landing = () => import('@/components/Landing/LandingContainer');
const Cabinet = () => import('@/components/Cabinet/Cabinet');
const MyOrders = () => import('@/components/Cabinet/MyOrders');
const OrderAbout = () => import('@/components/Cabinet/OrderAbout');
const ContainerTracking = () => import('@/components/Cabinet/ContainerTracking');
const SpecialOffer = () => import('@/components/Cabinet/SpecialOffer');
const SpecialOfferList = () => import('@/components/Cabinet/SpecialOfferList');
const SpecialOfferEdit = () => import('@/components/Cabinet/SpecialOfferEdit');
const MyBonuses = () => import('@/components/Cabinet/MyBonuses');
const AvailableOffers = () => import('@/components/Cabinet/AvailableOffers');

import store from '../store';

Vue.use(Router);

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: PageContainer,
      children: [
        {
          path: 'cabinet',
          name: 'cabinet',
          component: Cabinet,
          meta: { requiresAuth: true, scrollToTop: true }
        },
        {
          path: 'my-orders',
          name: 'my-orders',
          component: MyOrders,
          meta: { requiresAuth: true, scrollToTop: true }
        },
        {
          path: 'my-bonuses',
          name: 'my-bonuses',
          component: MyBonuses,
          meta: { requiresAuth: true, scrollToTop: true }
        },
        {
          path: 'order/about/:id',
          name: 'order-about',
          component: OrderAbout,
          meta: { requiresAuth: true, scrollToTop: true }
        },
        {
          path: 'tracking/container/:id',
          name: 'container-tracking',
          component: ContainerTracking,
          meta: { requiresAuth: true, scrollToTop: true }
        },
        {
          path: 'special/:id',
          name: 'special',
          component: SpecialOffer,
          meta: { requiresAuth: true, scrollToTop: true }
        },
        {
          path: 'manager/special/all',
          name: 'special-list',
          component: SpecialOfferList,
          meta: { requiresAuth: true, scrollToTop: true }
        },
        {
          path: 'manager/special/item/:id',
          name: 'special-edit',
          component: SpecialOfferEdit,
          meta: { requiresAuth: true, scrollToTop: true }
        },
        {
          path: 'cabinet/offers/my',
          name: 'available-offers',
          component: AvailableOffers,
          meta: { requiresAuth: true, scrollToTop: true,
                  isVip: true }
        },
        {
          path: 'catalog',
          name: 'catalog',
          component: Catalog,
          meta: { scrollToTop: true }
        },
        {
          path: 'manager',
          name: 'manager',
          component: ManagerCard,
          meta: { requiresAuth: true },
        },
        {
          path: 'manager/container/:id',
          name: 'container-edit',
          component: ContainerEdit,
          meta: { requiresAuth: true, scrollToTop: true }
        },
        {
          path: 'container/:translate',
          name: 'container-sale',
          component: ContainerSale,
          meta: { scrollToTop: true }
        },
        {
          path: 'cart',
          component: Cart,
          children:
          [
            {
              path: 'check',
              name: 'cart-check',
              component: CartCheck,
              meta: { scrollToTop: true }
            },
            {
              path: 'execute',
              name: 'cart-execute',
              component: CartExecute,
              meta: { requiresFilledCart: true, scrollToTop: true }
            },
            {
              path: 'pay',
              name: 'cart-pay',
              component: CartPay,
              meta: { requiresFilledCart: true, scrollToTop: true }
            }
          ]
        },
        {
          path: 'faq',
          name: 'faq',
          component: Faq,
          meta: { scrollToTop: true }
        },
        {
          path: '',
          redirect: 'catalog'
        }
      ]
    },
    {
      path: '/landing',
      name: 'landing',
      component: Landing,
      meta: { scrollToTop: true }
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      let position = {};
      if (to.matched.some(m => m.meta.scrollToTop)) {
        position = { x: 0, y: 0 };
      }
      return position;
    }
  }
});

router.beforeEach((to, from, next) => {
  store.dispatch('auth/verify');
  if (to.matched.some(record => record.meta.requiresAuth) && !store.getters['auth/isAuthorized']) {
    if (store.getters['auth/isAuthorized']) {
      next();
    } else {
      if (from.name === null) {
        next();
      }
      store.dispatch('modal/show', { type: 'login' });
      store.commit('auth/setNextRoute', to.path);
    }
  } else if (to.matched.some(record => record.meta.requiresFilledCart)
      && store.state.cart.containersCount === 0) {
    if (from.name === null) {
      router.replace({ name: 'cart-check' });
    } else {
      store.dispatch('modal/show', { type: 'check' });
    }
  } else if (to.matched.some(record => record.meta.isVip) && !store.getters['auth/isVip']) {
    if (store.getters['auth/isVip']) {
      next();
    } else {
      if (from.name === null) {
        next();
      }
      store.commit('auth/setNextRoute', to.path);
    }
  } else {
    next();
  }

});

export default router;
