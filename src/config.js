const debug = process.env.NODE_ENV !== 'production';

export const root = debug ? 'http://localhost:8000' : '';
