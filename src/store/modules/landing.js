import Vue from 'vue';

export default {
  namespaced: true,
  actions: {
    sendRequest ({ commit }, data) {
      return Vue.http.post('main/help/', data, {headers: {}});
    }
  }
}
