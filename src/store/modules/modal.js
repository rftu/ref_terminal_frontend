export default {
  namespaced: true,
  state: {
    isVisible: false,
    type: null,
    params: {}
  },
  mutations: {
    makeVisible (state, { type, params = {} }) {
      state.isVisible = true;
      state.type = type;
      state.params = params;
    },
    makeInvisible (state) {
      state.isVisible = false;
      state.params = {};
    },
    setOnHideFunction (state, onHide) {
      state.params.onHide = onHide;
    }
  },
  actions: {
    show ({ commit }, { type, params }) {
      commit('makeVisible', { type, params });
    },
    hide ({ state, commit }) {
      if (state.isVisible) {
        if (state.params.onHide) {
          state.params.onHide();
        }
        commit('makeInvisible');
      }
    }
  }
}
