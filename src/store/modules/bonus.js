import Vue from 'vue';
import { SnotifyService } from 'vue-snotify';

export default {
  namespaced: true,
  state: {
    bonuses: []
  },
  getters: {},
  mutations: {
    setBonuses (state, bonuses) {
      state.bonuses = bonuses;
    },
    appendBonuses (state, bonuses) {
      state.bonuses = state.bonuses.concat(bonuses)
    }
  },
  actions: {
    getBonuses ({ rootState, commit }, id = rootState.auth.person.id) {
      if (!Array.isArray(id)) {
        id = [ id ];
      }
      for (let i = 0; i < id.length; i++) {
        Vue.http.get('main/manager/' + id[i] + '/bonuses/', {
          headers: {
            // Pragma: 'no-cache',
            // Expires: '-1',
            // 'Cache-Control': 'no-cache'
          }
        }).then(response => {
          if (i > 0) {
            commit('appendBonuses', response.body);
          } else {
            commit('setBonuses', response.body);
          }
        }, response => {
          SnotifyService.error('Не удалось загрузить бонусы.', 'Ошибка!');
        });
      }
    },
    addBonus ({ rootState, dispatch }, bonus) {
      Vue.http.post('main/manager/bonuses/', bonus, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(response => {
        dispatch('getBonuses');
        dispatch('modal/hide', {}, { root: true })
      }, response => {
        if (response.status === 403) {
          SnotifyService.error('У вас нет прав для создания бонуса.', 'Ошибка!');
        } else {
          SnotifyService.error('Не удалось создать бонус! Попробуйте позже.', 'Ошибка!');
        }
      })
    },
    updateBonus ({ rootState, dispatch }, { id, bonus }) {
      Vue.http.post('main/manager/bonuses/' + id, bonus, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(response => {
        dispatch('getBonuses');
        dispatch('modal/hide', {}, { root: true });
      }, response => {
        SnotifyService.error('Не удалось сохранить изменения! Попробуйте позже.', 'Ошибка!');
      })
    },
    deleteBonus ({ rootState, dispatch }, id) {
      Vue.http.delete('main/manager/bonuses/' + id, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(response => {
        dispatch('getBonuses');
        SnotifyService.success('', 'Бонус удалён!');
      }, response => {
        SnotifyService.error('Не удалось удалить бонус! Попробуйте позже.', 'Ошибка!');
      })
    }
  }
}
