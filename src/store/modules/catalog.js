import Vue from 'vue';
import { SnotifyService } from 'vue-snotify';

export default {
  namespaced: true,
  state: {
    activeParams: {
      filters: {
        catalog: 'all',
        price: [],
        types: [],
        sizes: [],
        city: { name: 'Все города' }
      },
      sort: 'status',
      searchNumber: null
    },
    containers: [],
    containersCount: 0,
    loadedContainersCount: 0,
    loadParent: '',
    filtersObject: {}
  },
  mutations: {
    updateActiveFilters (state, data) {
      state.activeParams.searchNumber = null;
      Object.assign(state.activeParams.filters, data);
    },
    setActiveSort (state, sort) {
      state.activeParams.sort = 'status,' + sort;
    },
    setSearchNumber (state, number) {
      state.activeParams.searchNumber = number || null;
    },
    updateContainers (state, { list, rewrite }) {
      if (rewrite) {
        state.containers = list;
      } else {
        state.containers = state.containers.concat(list);
      }
    },
    setContainersCount (state, count) {
      state.containersCount = count;
    },
    setLoadParams(state, { count, parent }) {
      state.loadedContainersCount = count;
      state.loadParent = parent;
    },
    setFiltersObject(state, object) {
      state.filtersObject = object;
    }
  },
  actions: {
    getContainers ({ state, rootState, commit, dispatch }, {
      limit = state.loadedContainersCount, offset = 0, filter = true, parent = state.loadParent
    }) {
      let count = state.loadedContainersCount + limit;
      if (filter) {
        dispatch('updateFiltersObject');
        count = limit;
      }
      commit('setLoadParams', { count, parent });
      let data = {
        type: rootState.filter.type,
        filters: state.filtersObject
      };
      let params = {
        limit,
        offset,
        sort: state.activeParams.sort,
        reverse: state.activeParams.sort === rootState.container.property.discount.name ? 1 : 0
      };
      let success = (response) => {
        commit('setContainersCount', response.body.count);
        commit('updateContainers', { list: response.body.results, rewrite: filter });
        document.dispatchEvent(new Event('content-loaded'));
      };
      let error = (response) => {
        SnotifyService.error('Не удалось загрузить контейнеры.', 'Ошибка!');
      };
      let type = parent ? parent.charAt(0).toUpperCase() + parent.slice(1) : 'All';
      return dispatch('get' + type + 'Containers', { data, params, success, error });
    },
    getAllContainers ({}, { data, params, success, error }) {
      return Vue.http.post('products/container/filter/', data, {
        params
      }).then(success, error);
    },
    getManagerContainers ({ rootState }, { data, params, success, error }) {
      let id = rootState.auth.person.id;
      return Vue.http.post('products/container/manager/' + id, data, {
        params
      }).then(success, error);
    },
    getSelfContainers ({}, { data, params, success, error }) {
      return Vue.http.post('products/container/manager/my_containers/', data, {
        params
      }).then(success, error);
    },
    deleteContainer ({ state, rootState, dispatch }, id) {
      return Vue.http.delete('products/container/manager/container/' + id, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(response => {
        SnotifyService.success('', 'Контейнер удалён!');
        dispatch('getContainers', {
          // limit: state.loadedContainersCount,
          // offset: 0,
          // filter: true,
          // parent: state.loadParent
        });
      }, response => {
        SnotifyService.error('Не удалось удалить контейнер.', 'Ошибка!');
      });
    },
    updateFiltersObject ({ state, rootState, commit }) {
      let filters = {};
      let a = state.activeParams.filters;
      let p = rootState.container.property;

      if (state.activeParams.searchNumber !== null) {
        filters[p.number.name] = state.activeParams.searchNumber;
        commit('setFiltersObject', filters);
        return;
      }

      let selectedFilters = a.types.concat(a.sizes);
      for (let i = 0; i < selectedFilters.length; i++) {
        let t = selectedFilters[i];
        if (filters[t.field]) {
          if (t.field == 'tip') {
              filters[t.field].push(t.property);
          }
          else {
              filters[t.field].push(t.name);
          }
        } else {
          if (t.bool) {
            filters[t.field] = [ true ];
          } else {
            if (t.field == 'tip') {
                filters[t.field] = [t.property];
            }
            else {
                filters[t.field] = [t.name];
            }
          }
        }
      }
      if (a.price.length > 0) {
        let price = a.price[0] + ',' + a.price[1]
        if (a.catalog === 'rent') {
          filters[p.rentPriceMonth.filter] = price;
        } else if (a.catalog === 'auction') {
          console.log(p.auctionStartPrice.filter, price);
          filters[p.auctionStartPrice.filter] = price;
        } else {
          filters[p.priceSale.filter] = price;
        }
      }
      switch (a.catalog) {
        case 'sale':
          filters[p.sale.name] = [ true ];
          break;
        case 'rent':
          filters[p.rent.name] = [ true ];
          break;
        case 'auction':
          filters[p.auction.name] = [ true ];
          break;
        case 'manager':
          break;
      }
      if (a.city.content) {
        filters[p.city.name] = { [p.city.name]: a.city.name };
      }
      commit('setFiltersObject', filters);
    }
  }
}
