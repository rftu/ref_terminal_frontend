import Vue from 'vue';
import { SnotifyService } from 'vue-snotify';

export default {
  namespaced: true,
  state: {
    sortList: [],
    isSortListLoaded: false
  },
  mutations: {
    setSorts (state, sorts) {
      state.sortList = sorts;
      state.isSortListLoaded = true;
    }
  },
  actions: {
    getSortList ({ state, rootState, commit }) {
      if (state.isSortListLoaded) { return; }
      Vue.http.get('products/sort/' + rootState.filter.type).then(response => {
        commit('setSorts', response.body);
      }, response => {
        SnotifyService.error('Не удалось загрузить данные сортировки.', 'Ошибка!');
      });
    }
  }
}

