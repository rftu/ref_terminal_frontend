import Vue from 'vue';
import { SnotifyService } from 'vue-snotify';
import router from '../../router';

export default {
  namespaced: true,
  state: {
    container: {
      images: []
    },
    fields: {},
    data: {},
    isLoading: false,
    property: {
      gDescription: {
        name: 'opisanie-garantii',
        type: 'str_fields',
        required: true
      },
      description: {
        name: 'opisanie',
        type: 'str_fields',
        required: true
      },
      innerSize: {
        name: 'vnutrennie-razmery',
        type: 'str_fields',
        required: true
      },
      number: {
        name: 'nomer-konteinera',
        type: 'str_fields',
        required: true
      },
      rentPriceMonth: {
        name: 'stoimost-arendy-v-mesiats',
        type: 'str_fields',
        filter: 'tsena-arendy-v-mesiats'
      },
      price: {
        name: 'tsena',
        type: 'int_fields'
      },
      priceSale: {
        name: 'tsena-so-skidkoi',
        type: 'int_fields',
        filter: 'tsena',
        required: true
      },
      gTime: {
        name: 'srok-garantii',
        type: 'int_fields',
        required: true
      },
      rentPriceYear: {
        name: 'stoimost-arendy-v-god',
        type: 'int_fields',
        filter: 'tsena-arendy-v-god'
      },
      auctionStartPrice: {
        name: 'nachalnaya-tsena-aukcion',
        type: 'int_fields',
        filter: 'nachalnaya-tsena-aukcion',
        required: true
      },
      auctionMinPrice: {
        name: 'minimaljnaya-stavka-aukcion',
        type: 'int_fields',
      },
      auctionRedemptionPrice: {
        name: 'tsena-vykupa-aukcion',
        type: 'int_fields',
      },
      auctionTime: {
        name: 'vremya-provedeniya-aukciona',
        type: 'int_fields',
        required: true
      },
      weight: {
        name: 'ves-tary',
        type: 'int_fields',
        required: true
      },
      year: {
        name: 'god',
        type: 'int_fields',
        required: true
      },
      volume: {
        name: 'vmestimost-v-m3',
        type: 'int_fields',
        required: true
      },
      generalPrice: {
        name: 'obshchaia-tsena',
        type: 'int_fields'
      },
      guarantee: {
        name: 'garantiia',
        type: 'bool_fields',
        required: true
      },
      rent: {
        name: 'arenda',
        type: 'bool_fields'
      },
      sale: {
        name: 'prodazha',
        type: 'bool_fields'
      },
      auction: {
        name: 'auction',
        type: 'bool_fields'
      },
      published: {
        name: 'publish',
        type: 'bool_fields',
        required: true
      }, 
      usedInRF: {
        name: 'ispolzovalsia-v-rf',
        type: 'bool_fields',
        required: true
      },
      discount: {
        name: 'skidka',
        type: 'bool_fields',
        required: true
      },
      state: {
        name: 'sostoianie',
        type: 'select_fields',
        required: true
      },
      size: {
        name: 'razmer',
        type: 'select_fields',
        required: true
      },
      type: {
        name: 'tip',
        type: 'select_fields',
        required: true
      },
      status: {
        name: 'status',
        type: 'select_fields',
        required: true
      },
      city: {
        name: 'gorod',
        type: 'select_fields',
        custom: 'drugoe-mesto',
        required: true
      },
      model: {
        name: 'model',
        type: 'select_fields',
        custom: 'drugaia-model',
        customType: 'str_fields'
      },
      statementTC: {
        name: 'akt-tekhnicheskogo-sostoianiia',
        type: 'file_fields',
        required: true
      }
    }
  },
  mutations: {
    setContainer (state, container) {
      state.container = {
        id: container.id,
        type: container.type,
        images: container.images,
        name: container.name,
        manager: container.manager,
        auction_params: container.auction_params,
        seo_title: container.seo_title,
        seo_description: container.seo_description,
        seo_keywords: container.seo_keywords,
      };
      Object.assign(state.container, container.property_json);
    },
    setContainerFields (state, fields) {
      state.fields = fields;
    },
    setData (state, data) {
      let d = {};
      Object.assign(d, data);
      state.data = d;
    },
    initContainer (state) {
      state.container = {
        images: [],
      };
    },
    setLoadingFlag (state, flag) {
      state.isLoading = flag;``
    }
  },
  actions: {
    addContainer ({ state, rootState, commit, dispatch }, data) {
      dispatch('getContainerFields', data);
      return Vue.http.post('products/container/manager/add_container/', {
        name: state.data.name,
        type: 1,
        fields: state.fields
      }, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(response => {
        SnotifyService.success('', 'Контейнер сохранён!');
        commit('setContainer', response.body);
        router.replace({ name: 'container-edit', params: { id: response.body.id } });
        dispatch('catalog/getContainers', {}, { root: true });
      }, response => {
        var msg = 'Не удалось сохранить контейнер!';
        if (response.hasOwnProperty('bodyText')) {
          msg = JSON.parse(response.bodyText).detail;
        }
        SnotifyService.error('', msg);
      });
    },
    updateContainer ({ state, rootState, commit, dispatch }, data) {
      dispatch('getContainerFields', data);
      return Vue.http.put('products/container/manager/container/' + state.container.id, {
        name: state.data.name,
        type: state.container.type,
        fields: state.fields
      }, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(response => {
        SnotifyService.success('', 'Изменения сохранёны!');
        commit('setContainer', response.body);
        dispatch('catalog/getContainers', {}, { root: true });
      }, () => {
        SnotifyService.error('', 'Не удалось сохранить изменения!');
      });
    },
    getEditContainer ({ rootState, commit }, id) {
      return Vue.http.get('products/container/manager/container/' + id, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(response => {
        commit('setContainer', response.body);
      }, () => {
        SnotifyService.error('Не удалось загрузить контейнер.', 'Ошибка!');
      });
    },
    getContainer ({ commit }, translate) {
      return Vue.http.get('products/container/' + translate).then(response => {
        commit('setContainer', response.body);
      }, () => {
        SnotifyService.error('Не удалось загрузить контейнер.', 'Ошибка!');
      });
    },
    getContainerFields ({ state, commit }, newData = {}) {
      let fields = {
        str_fields: [],
        int_fields: [],
        bool_fields: [],
        select_fields: [],
        file_fields: []
      };
      let data = {};
      Object.assign(data, state.data, newData);
      if (Object.getOwnPropertyNames(newData).length > 0) {
        data.published = false;
      }
      for (let key in state.property) {
        if (state.property.hasOwnProperty(key)) {
          if (data[key] !== undefined && data[key] !== null) {
            if (key === 'city') {
              let content = { ...data.city };
              let custom = typeof(data.address) === 'string';
              let addressObj = custom ? content.content[0] : data.address;
              let addressStr = custom ? data.address : addressObj.name;
              content.name = content.translate_name;
              content.content = addressObj;
              fields.select_fields.push({
                name: state.property.city.name,
                content
              });
              fields.str_fields.push({
                name: state.property.city.custom,
                content: addressStr
              });
            }
            else {
              if (state.property[key].custom && typeof(data[key]) === 'string') {
                fields[state.property[key].customType].push({
                  name: state.property[key].custom,
                  content: data[key]
                });
              } else {
                if (state.property[key].custom) {
                  fields[state.property[key].customType].push({
                    name: state.property[key].custom,
                    content: null
                  });
                }
                fields[state.property[key].type].push({
                  name: state.property[key].name,
                  content: data[key]
                });
              }

            }
          }
        }
      }
      if (fields.file_fields.length === 0) {
        delete fields.file_fields;
      }
      commit('setContainerFields', fields);
    },
    addImage ({ state, rootState, dispatch }, form) {
      return Vue.http.post('products/images/add/', form, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(() => {
        SnotifyService.success('', 'Изображение добавлено!');
        dispatch('getEditContainer', state.container.id);
        dispatch('catalog/getContainers', {}, { root: true });
      }, () => {
        SnotifyService.error('Не удалось загрузить изображение!', 'Ошибка!');
      })
    },
    deleteImage ({ state, rootState, dispatch }, id) {
      return Vue.http.delete('products/images/' + id, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(() => {
        SnotifyService.success('', 'Изображение удалено!');
        dispatch('getEditContainer', state.container.id);
        dispatch('catalog/getContainers', {}, { root: true });
      }, () => {
        SnotifyService.error('Не удалось удалить изображение!', 'Ошибка!');
      })
    },
    setMainImage ({ state, rootState, dispatch }, id) {
      return Vue.http.post('products/images/' + id, {
        headers: { Authorization: 'Token ' + rootState.auth.token }
      }).then(() => {
        dispatch('getEditContainer', state.container.id);
        dispatch('catalog/getContainers', {}, { root: true });
      }, () => {
        SnotifyService.error('', 'Ошибка!');
      })
    },
    createAuctionBet({ commit, dispatch }, data) {
      Vue.http.put('main/order/create_bet/', data).then(response => {
        SnotifyService.success('', 'Спасибо, ставка принята!');
        commit('setContainer', response.body.data);
        dispatch('modal/hide', {}, { root: true });
      }, response => {
        var msg = response.body.err || 'Попробуйте позже.';
        SnotifyService.error(msg, 'Ошибка!');
        if (response.body.data !== undefined) {
          commit('setContainer', response.body.data);
        }
        if (response.status !== 412) {
          dispatch('modal/hide', {}, { root: true });
        }
      })
    }
  }
}
