import Vue from 'vue';
import { SnotifyService } from 'vue-snotify';
import router from '../../router';


const AUTH_TOKEN_KEY = 'ref_auth_token';
const getToken = () => localStorage.getItem(AUTH_TOKEN_KEY);


export default {
    namespaced: true,
    state: {
        shortOrdersInfo: [],
        orderInfo: {},
        saleContainers: [],
        rentContainers: [],
        auctionContainers: [],
        specialContainers: [],
        currentSpecialContainers: [],
        totalCost: 0,
        totalWithDiscount: 0,
        trackingInfo: {},
        specialOffers: [],
        specialOffer: {},
        isLoaded: false
    },
    mutations: {
        setMyOrdersInfo (state, info) {
            state.shortOrdersInfo = info;
        },
        setOrderInfo (state, info) {
            state.orderInfo = info;
        },
        setContainers (state, { sale, rent, auction, special }) {
            state.saleContainers = sale;
            state.rentContainers = rent;
            state.auctionContainers = auction;
            state.currentSpecialContainers = special;
        },
        setSpecialOffers (state, { offers }) {
            state.specialOffers = offers;
        },
        setSpecialOffer (state, { offer }) {
            state.specialOffer = offer;
        },
        setSpecialContainers (state, containers) {
          state.specialContainers = containers;
        },
        setTotalCost (state, { total_cost, total_with_discount }) {
            state.totalCost = total_cost;
            state.totalWithDiscount = total_with_discount;
        },
        setTrackingInfo (state, tracking_info) {
          state.trackingInfo = tracking_info;
        },
        setIsLoaded(state, value) {
          state.isLoaded = value;
        },
        initLoaded(state) {
            state.isLoaded = false;
            state.saleContainers = [];
            state.rentContainers = [];
            state.auctionContainers = [];
            state.orderInfo = {};
            state.trackingInfo = {};
            state.specialContainers = [];
            state.currentSpecialContainers = [];
        }
    },
    actions: {
        initLoaded({state, commit}) {
            commit('initLoaded');
        },
        parseMyOrders({ state, commit }, body){
            commit('setMyOrdersInfo', body);
        },
        parseOrder({ state, commit }, body){
            commit('setOrderInfo', body);
        },
        parseSpecialOffers({ dispatch, state, commit }, offers) {
          commit('setSpecialOffers', {offers: offers});
        },
        parseSpecialOffer({ dispatch, state, commit }, offer) {
          commit('setSpecialOffer', {offer: offer});
        },
        parseFullSpecialOffer({ dispatch, state, commit }, body) {
          commit('setContainers', {special: body.containers});
          commit('setTotalCost', body);
        },
        parseTracking({ state, commit }, body) {
            commit('setTrackingInfo', body);
        },
        getMyOrders ({ commit, dispatch }) {
            commit('setIsLoaded', false);
            Vue.http.get('main/order/my/', {
            }).then(response => {
              dispatch('parseMyOrders', response.body);
              commit('setIsLoaded', true);
            }, response => {
              if (response.status !== 500) {
                SnotifyService.error('Не удалось получить заказы.', 'Ошибка!');
              }
              commit('setIsLoaded', true);
            });
        },
        getOrder ({ commit, dispatch }, pk) {
            dispatch('initLoaded');
            Vue.http.get('main/order/my/' + pk, {
            }).then(response => {
              dispatch('parseOrder', response.body);
              commit('setIsLoaded', true);
            }, response => {
              if (response.status !== 500) {
                SnotifyService.error('Не удалось получить заказ.', 'Ошибка!');
                commit('setIsLoaded', true);
              }
            });
        },
        getTracking ({ commit, dispatch }, pk) {
          dispatch('initLoaded');
          Vue.http.get('main/tracking/' + pk, {
            headers: { Authorization: 'Token ' + getToken() }
          }).then(response => {
            dispatch('parseTracking', response.body);
            commit('setIsLoaded', true);
          }, response => {
            if (response.status !== 500) {
              dispatch('parseTracking', {});
              commit('setIsLoaded', true);
            }
          });
        },
        getSpecialContainers ({ state, commit }) {
          commit('setIsLoaded', false);
          return Vue.http.get('main/special/containers/').then(response => {
            commit('setSpecialContainers', response.body);
            commit('setIsLoaded', true);
          }, () => {
            commit('setSpecialContainers', []);
            commit('setIsLoaded', true);
          });
        },
        getSpecialOffer ({ commit, dispatch }, pk) {
            commit('setIsLoaded', false);
            Vue.http.get('main/special/' + pk, {
            }).then(response => {
              dispatch('parseSpecialOffer', response.body);
              commit('setIsLoaded', true);
            }, response => {
              if (response.status !== 500) {
                commit('setIsLoaded', true);
                SnotifyService.error('Не удалось получить оптовое предложение.', 'Ошибка!');
              }
            });
        },
        getFullSpecialOffer ({ commit, dispatch }, pk) {
            commit('setIsLoaded', false);
            Vue.http.get('main/special/get/' + pk, {
            }).then(response => {
              console.log(response);
              dispatch('parseFullSpecialOffer', response.body);
              commit('setIsLoaded', true);
            }, response => {
              if (response.status !== 500) {
                commit('setIsLoaded', true);
                SnotifyService.error('Не удалось получить оптовое предложение.', 'Ошибка!');
              }
            });
        },
        getSpecialOffers({ commit, dispatch }) {
          commit('setIsLoaded', false);
          Vue.http.get('main/special/all/', {
            }).then(response => {
              dispatch('parseSpecialOffers', response.body);
              commit('setIsLoaded', true);
            }, response => {
              if (response.status !== 500) {
                commit('setIsLoaded', true);
                SnotifyService.error('Не удалось получить оптовые предложения.', 'Ошибка!');
              }
            });
        },
        updateSpecialOffer ({ state, rootState, commit, dispatch }, data) {
          commit('setIsLoaded', false);
          return Vue.http.put('main/special/item/' + state.specialOffer.id,
          data, {
            headers: { Authorization: 'Token ' + rootState.auth.token }
          }).then(response => {
            SnotifyService.success('', 'Изменения сохранёны!');
            dispatch('parseSpecialOffer', response.body);
            commit('setIsLoaded', true);
          }, () => {
            commit('setIsLoaded', true);
            SnotifyService.error('', 'Не удалось сохранить изменения!');
          });
        },
        addSpecialOffer ({ state, rootState, commit, dispatch }, data) {
          commit('setIsLoaded', false);
          return Vue.http.post('main/special/item/add/',
          data, {
            headers: { Authorization: 'Token ' + rootState.auth.token }
          }).then(response => {
            SnotifyService.success('', 'Изменения сохранёны!');
            router.replace({ name: 'special-edit', params: { id: response.body.id } });
            dispatch('parseSpecialOffer', response.body);
            commit('setIsLoaded', true);
          }, response => {
            var msg = 'Не удалось сохранить изменения!';
            if (response.hasOwnProperty('bodyText')) {
              msg = JSON.parse(response.bodyText).detail;
            }
            commit('setIsLoaded', true);
            SnotifyService.error('', msg);
          });
        },
        removeSpecialOffer ({ state, rootState, commit, dispatch }) {
          commit('setIsLoaded', false);
          return Vue.http.delete('main/special/item/' + state.specialOffer.id, {
            headers: { Authorization: 'Token ' + rootState.auth.token }
          }).then(response => {
            SnotifyService.success('', 'Предложение удалено!');
            dispatch('parseSpecialOffer', {});
            commit('setIsLoaded', true);
            router.replace({ name: 'special-list'});
          }, () => {
            commit('setIsLoaded', true);
            SnotifyService.error('', 'Не удалось внести изменения!');
          });
        },
        getAvailableSpecialOffers({ commit, dispatch }) {
          commit('setIsLoaded', false);
          Vue.http.get('main/special/available/', {
            }).then(response => {
              dispatch('parseSpecialOffers', response.body);
              commit('setIsLoaded', true);
            }, response => {
              if (response.status !== 500) {
                commit('setIsLoaded', true);
                SnotifyService.error('Не удалось получить оптовые предложения.', 'Ошибка!');
              }
            });
        },
    }
}