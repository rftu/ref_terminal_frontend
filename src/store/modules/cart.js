import Vue from 'vue';
import { SnotifyService } from 'vue-snotify';

const TOKEN_KEY = 'order_id';

const setToken = (token) => localStorage.setItem(TOKEN_KEY, token);
const getToken = () => localStorage.getItem(TOKEN_KEY);
const removeToken = () => localStorage.removeItem(TOKEN_KEY);

export default {
  namespaced: true,
  state: {
    orderInfo: {},
    saleContainers: [],
    rentContainers: [],
    saleContainersId: [],
    rentContainersId: [],
    containersCount: 0,
    containersCountWord: '',
    fields: {
      'private': [
        'type',
        'inn',
        'snils',
        'address',
        'phone',
        'email',
        'fio',
        'propiska',
        'pasport'
      ],
      'ip': [
        'inn',
        'type',
        'ogrnip',
        'address',
        'phone',
        'email',
        'fio',
        'propiska',
        'pasport'
      ],
      'organization': [
        'org_name',
        'inn',
        'type',
        'kpp',
        'checking_account',
        'correspondent_account',
        'foreign_currency_account',
        'bank',
        'bik_bank',
        'kpp_bank',
        'address',
        'city',
        'post_adress',
        'phone',
        'email',
        'fio',
        'dolznost',
        'okved',
        'okpo',
        'oktmo'
      ]
    }
  },
  getters: {
    isOrderCreated: () => getToken() !== null,
    containersCountString: (state) => state.containersCount + ' ' + state.containersCountWord
  },
  mutations: {
    setOrderInfo (state, info) {
      state.orderInfo = info;
    },
    setContainers (state, { sale, rent, auction }) {
      state.saleContainers = sale;
      state.rentContainers = rent;
      state.auctionContainers = auction;
    },
    setContainersCount (state, count) {
      state.containersCount = count;
    },
    setContainersCountWord (state, word) {
      state.containersCountWord = word;
    },
    setContainersId (state, { sale, rent, auction }) {
      state.saleContainersId = sale;
      state.rentContainersId = rent;
      state.auctionContainersId = auction;
    }
  },
  actions: {
    getOrder ({ dispatch }) {
      Vue.http.get('main/order/' + getToken(), {
        // headers: {
        //   //Pragma: 'no-cache',
        //   //Expires: '-1',
        //   // 'Cache-Control': 'no-cache'
        // }
      }).then(response => {
        dispatch('parseOrder', response.body);
      }, response => {
        if (response.status !== 500) {
          dispatch('createOrder');
        }
      });
    },
    createOrder ({ dispatch }) {
      Vue.http.post('main/order/create/', {}).then(response => {
        setToken(response.body.pk);
        dispatch('parseOrder', response.body);
      }, response => {
        SnotifyService.error('Не удалось создать корзину.', 'Ошибка!');
      });
    },
    parseOrder ({ state, commit }, body) {
      let saleCount = body.sale_trade_items ? body.sale_trade_items.length : 0;
      let rentCount = body.rent_trade_items ? body.rent_trade_items.length : 0;
      let auctionCount = body.auction_trade_items ? body.auction_trade_items.length : 0;
      commit('setContainersCount', saleCount + rentCount + auctionCount);

      let saleList = [],
          rentList = [],
          auctionList = [];
      for (let i = 0; i < saleCount; i++) {
        saleList.push(body.sale_trade_items[i].container.id);
      }
      for (let i = 0; i < rentCount; i++) {
        rentList.push(body.rent_trade_items[i].container.id);
      }
      for (let i = 0; i < auctionCount; i++) {
        auctionList.push(body.auction_trade_items[i].container.id);
      }
      let word;
      let number = state.containersCount % 100;
      let endings = ['контейнер', 'контейнера', 'контейнеров'];
      if (state.containersCount === 0) {
        word = endings[2];
      }
      else if (number >= 11 && number <= 19) {
        word = endings[2];
      }
      else {
        number = number % 10;
        switch (number) {
          case (1):
            word = endings[0];
            break;
          case (2):
          case (3):
          case (4):
            word = endings[1];
            break;
          default:
            word = endings[2];
        }
      }
      commit('setOrderInfo', {
        price: body.total_price,
        priceWithDiscount: body.price_with_all_bonuses,
        rentPriceMonth: body.total_price_rent_month,
        rentPriceYear: body.total_price_rent_year,
        auctionPrice: body.total_price_auction,
        bonuses: body.bonuses,
        properties: body.properties
      });
      commit('setContainers', { sale: body.sale_trade_items, rent: body.rent_trade_items, auction: body.auction_trade_items });
      commit('setContainersId', { sale: saleList, rent: rentList, auction: auctionList });
      commit('setContainersCountWord', word);
    },
    addOrRemoveContainers ({ dispatch }, { add = [], remove = [], mute }) {
      Vue.http.put('main/order/add_containers/' + getToken(), {
        add_items: add,
        remove_items: remove
      }).then(response => {
        dispatch('getOrder');
        if (!mute) {
          if (add.length > 0) {
            var msg = add.length === 1 ? 'Контейнер добавлен в корзину!' : 'Контейнеры добавлены в корзину!'
            SnotifyService.success('', msg);
          } else {
            SnotifyService.success('', 'Контейнер удалён из корзины!');
          }
        }
      }, response => {
        SnotifyService.error('Не удалось добавить контейнер в корзину!', 'Ошибка!');
      });
    },
    updateOrderInfo ({ state, dispatch }, data) {
      let info = {};
      for (let i = 0; i < state.fields[data.type].length; i++) {
        info[(state.fields[data.type])[i]] = data[(state.fields[data.type])[i]];
      }
      Vue.http.put('main/order/update_user_info_in_order/' + getToken(), info).then(response => {
        dispatch('getOrder');
      }, response => {
        SnotifyService.error('Не удалось сохранить информацию.', 'Ошибка!');
      })
    },
    payForOrder ({ state, dispatch }) {
      Vue.http.get('main/order/pay/' + getToken()).then(response => {
        dispatch('modal/show', { type: 'pay', params: { result: 'success' } }, { root: true });
        dispatch('catalog/getContainers', {}, { root: true });
      }, response => {
        if (response.status === 412) {
          dispatch(
            'modal/show',
            { type: 'pay', params: { result: 'error', data: response.body.containers } },
            { root: true });
        } else {
          SnotifyService.error('Попробуйте позже.', 'Ошибка!');
        }
      });
    }
  }
}
