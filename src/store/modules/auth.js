import Vue from 'vue';
import { SnotifyService } from 'vue-snotify';

const TOKEN_KEY = 'ref_auth_token';
const VERIFY_TIMEOUT = 60;

const setToken = (token) => localStorage.setItem(TOKEN_KEY, token);
const setTemporaryToken = (token) => sessionStorage.setItem(TOKEN_KEY, token);
const getToken = () => localStorage.getItem(TOKEN_KEY) || sessionStorage.getItem(TOKEN_KEY);
const removeToken = () => localStorage.removeItem(TOKEN_KEY) || sessionStorage.removeItem(TOKEN_KEY);

export default {
  namespaced: true,
  state: {
    person: {},
    token: null,
    verificationTimerId: -1,
    nextRoute: null
  },

  getters: {
    isAuthorized: (state) => state.token !== null,
    isPersonEmpty: (state) => state.person.last_name === null || state.person.last_name === '',
    isVip: (state) => state.person.is_vip
  },

  mutations: {
    setPerson (state, person) {
      state.person = person;
    },
    setIsVIP (state, value) {
      state.person['is_vip'] = value;
    },
    setIsVipRequested (state, value) {
      state.person['is_vip_requested'] = value;
    },
    authorize (state, { token, timerId }) {
      state.verificationTimerId = timerId;
      state.token = token;
    },
    logout (state) {
      removeToken();
      clearTimeout(state.verificationTimerId);
      state.token = null;
      Vue.http.options.headers = {};
    },
    setNextRoute(state, route) {
      state.nextRoute = route;
    }
  },

  actions: {
    updatePersonalData ({ state, commit, dispatch }, person) {
      Vue.http.put('main/manager/self/', person, {
        headers: { Authorization: 'Token ' + state.token }
      }).then(response => {
        commit('setPerson', response.body);
        dispatch('modal/hide', {}, { root: true });
        SnotifyService.success('', 'Данные обновлены!');
      }, response => {
        SnotifyService.error('Не удалось выполнить сохранение. Попробуйте позже.', 'Ошибка!');
      })
    },
    updatePersonalPassword ({ state, commit, dispatch }, person) {
      if (person.old_password === person.new_password) {
        SnotifyService.error('Новый пароль совпадает с текущим.', 'Ошибка!');
        return;
      }
      Vue.http.put('main/manager/self/change_password/', person, {
        headers: { Authorization: 'Token ' + state.token }
      }).then(response => {
        dispatch('modal/hide', {}, { root: true });
        SnotifyService.success('', 'Данные обновлены!');
      }, response => {
        var message = response.body.hasOwnProperty('message') ? response.body.message : 'Не удалось выполнить сохранение. Попробуйте позже.'
        SnotifyService.error(message, 'Ошибка!');
      })
    },
    payVIP({ state, commit, dispatch }) {
        Vue.http.put('main/manager/self/pay_vip/', {
            headers: { Authorization: 'Token ' + state.token }
          }).then(response => {
            dispatch('modal/hide', {}, { root: true });
            commit('setIsVipRequested', true);
            SnotifyService.success('', 'Спасибо, заявку на подписка успешно создана!');
          }, response => {
            dispatch('modal/hide', {}, { root: true });
            var message = response.body.hasOwnProperty('message') ? response.body.message : 'Не удалось оплатить подписку. Попробуйте позже.'
            SnotifyService.error(message, 'Ошибка!');
          })
    },
    verify ({ commit, dispatch }) {
      if (getToken() !== null) {
        // Vue.http.post('', { token: getToken() }).then(response => {
        dispatch('authorize', getToken())
        // }, response => {
        //   commit('logout')
        // });
      }
    },
    authorize ({ commit, dispatch }, token) {
      Vue.http.options.headers = { Authorization: 'Token ' + token };
      commit('authorize', {
        token,
        // timerId: setTimeout(() => {
        //   dispatch('verify')
        // }, VERIFY_TIMEOUT * 1000),
      });
      Vue.http.get('main/manager/self/', {
        headers: { Authorization: 'Token ' + token }
      }).then(response => {
        commit('setPerson', response.body);
      }, response => {
        SnotifyService.error('Не удалось загрузить данные пользователя.', 'Ошибка!');
      });
    },
    login ({ commit, dispatch }, { username, password, remember }) {
      return Vue.http.post('main/token/auth_token/', { username, password }).then(response => {
        remember ? setToken(response.body.token) : setTemporaryToken(response.body.token);
        dispatch('authorize', response.body.token);
        dispatch('modal/hide', {}, { root: true });
        commit('setNextRoute', '/cabinet');
      }, response => {
        SnotifyService.error('', 'Неправильное имя пользователя или пароль!');
      });
    },
    register ({ commit, dispatch }, { email, password, password2 }) {
      if (password !== password2) {
        SnotifyService.error('Введённые пароли не совпадают!', 'Ошибка!');
        return;
      }
      Vue.http.post('main/accounts/', { email, password }).then(response => {
        dispatch('modal/show', { type: 'login', params: { email } }, { root: true });
      }, response => {
        if (response.body.email) {
          SnotifyService.error(response.body.email[0], 'Ошибка!');
        } else {
          SnotifyService.error('Попробуйте позже.', 'Ошибка!');
        }
      });
    }
  }
}
