import Vue from 'vue';
import { SnotifyService } from 'vue-snotify';

export default {
  namespaced: true,
  state: {
    isFieldListLoaded: false,
    sizeList: [],
    typeList: [],
    stateList: [],
    statusList: [],
    modelList: [],
    cityList: []
  },
  mutations: {
    setFields (state, fields) {
      for (let key in fields) {
        state[key + 'List'] = fields[key];
      }
      state.isFieldListLoaded = true;
    }
  },
  actions: {
    getFieldList ({ state, rootState, commit }) {
      if (state.isFieldListLoaded) { return; }
      return Vue.http.get('products/fields/' + rootState.filter.type).then(response => {
        let body = response.body;
        let fields = {};
        for (let i = 0; i < body.length; i++) {
          if (body[i].field_type === 'select') {
            for (let key in rootState.container.property) {
              if (rootState.container.property[key].name === body[i].translate) {
                fields[key] = [];
                for (let j = 0; j < body[i].content.length; j++) {
                  if (key !== 'city' || body[i].content[j].content.length > 0) {
                    fields[key].push(body[i].content[j]);
                  }
                }
                break;
              }
            }
          }
        }
        commit('setFields', fields);
      }, response => {
        commit('setFields', {});
        SnotifyService.error('Попробуйте перезагрузить страницу или зайти позже.', 'Не удалось загрузить поля контейнера!');
      });
    }
  }
}

