import Vue from 'vue';
import { SnotifyService } from 'vue-snotify';

export default {
  namespaced: true,
  state: {
    type: '',
    isFilterListLoaded: false,
    typeList: [],
    stateList: [],
    boolList: [],
    sizeList: [],
    cityList: [],
    priceRange: [0, 2000000]
  },
  mutations: {
    setFilters (state, filters) {
      for (let key in filters) {
        state[key + 'List'] = filters[key];
      }
      state.isFilterListLoaded = true;
    },
    setType (state, type) {
      state.type = type;
    }
  },
  actions: {
    getFilterList ({ state, commit }) {
      if (state.isFilterListLoaded) { return; }
      return Vue.http.get('products/types/').then(response => {
        commit('setType', response.body[0].translate);
        Vue.http.get('products/filters/' + state.type).then(response => {
          let b = response.body;
          let filters = { bool: [], type: [], size: [], state: [], city: [] };
          for (let i = 0; i < b.length; i++) {
            let f = b[i].field;
            let t = b[i].translate;
            if (f.type === 'boolean' && t !== 'arenda' && t !== 'prodazha') {
              filters.bool.push({ name: b[i].name, field: t, bool: true });
            } else if (f.field_type === 'select') {
              if (t === 'sostoianie') {
                let arr = b[i].field.content;
                for (let j = 0; j < arr.length; j++) {
                  filters.state.push({ name: arr[j].name, field: t });
                }
              } else if(t === 'tip') {
                let arr = b[i].field.content;
                for (let j = 0; j < arr.length; j++) {
                  filters.type.push({ name: arr[j].name, field: t, property: arr[j].property });
                }
              } else if (t === 'razmer') {
                let arr = b[i].field.content;
                for (let j = 0; j < arr.length; j++) {
                  filters.size.push(Object.assign(arr[j], { field: t }));
                }
              } else if (t === 'gorod') {
                let arr = b[i].field.content;
                filters.city.push({ name: 'Все города' });
                for (let j = 0; j < arr.length; j++) {
                  if (arr[j].content.length > 0) {
                    filters.city.push(Object.assign(arr[j], { field: t }));
                  }
                }
              }
            }
          }
          commit('setFilters', filters);
        }, response => {
          commit('setFilters', {});
          SnotifyService.error('Попробуйте перезагрузить страницу или зайти позже.', 'Не удалось загрузить фильтры!');
        });
      }, response => {
        // SnotifyService.error('Попробуйте перезагрузить страницу или зайти позже.', 'Ошибка!');
      });
    }
  }
}

