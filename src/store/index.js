import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import filter from './modules/filter';
import catalog from './modules/catalog';
import modal from './modules/modal';
import bonus from './modules/bonus';
import container from './modules/container';
import field from './modules/field';
import sort from './modules/sort';
import cart from './modules/cart';
import landing from './modules/landing';
import cabinet from './modules/cabinet';

const debug = process.env.NODE_ENV !== 'production';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: debug,
  modules: {
    auth,
    filter,
    catalog,
    modal,
    bonus,
    container,
    field,
    sort,
    cart,
    landing,
    cabinet
  }
});
