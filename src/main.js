import Vue from 'vue';

import App from './App';
import router from './router';
import store from './store';
import utils from './utils'

import objectFitImages from 'object-fit-images';

Vue.config.productionTip = false;

document.addEventListener('DOMContentLoaded', function () {
  var root = new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App }
  });

  root.$mount('#app');
  objectFitImages();
});
